package de.nazareth.klion.configserver.configurations

import org.springframework.context.annotation.Bean
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.provisioning.InMemoryUserDetailsManager
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@EnableWebSecurity
class WebSecurityConfig : WebMvcConfigurer {

    @Bean
    @Throws(Exception::class)
    fun userDetailsService(): UserDetailsService {
        val manager = InMemoryUserDetailsManager()
        manager.createUser(User.withDefaultPasswordEncoder()
                .username("tech_user")
                .password("2wsxzaq1")
                .roles("USER")
                .build())
        return manager
    }

    @Throws(Exception::class)
    protected fun configure(http: HttpSecurity) {
        http.authorizeRequests()
            .anyRequest().authenticated()
            .and()
            .formLogin()
            .and()
            .httpBasic()
    }
}